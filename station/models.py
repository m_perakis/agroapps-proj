from django.contrib.gis.db import models

from category.models import Category


class TimestampModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Station(TimestampModel):
    name = models.CharField(max_length=120, unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(max_length=250)
    longitude_latitude = models.PointField(unique=True)
    contact = models.CharField(max_length=120)

    class Meta:
        ordering = ["id"]
