from django.db import IntegrityError
from django.test import TestCase
from .models import Station
from category.models import Category


class StationTestCase(TestCase):
    def setUp(self):
        self.category = Category.objects.create(name="Dummy Category")
        Station.objects.create(
            name="Dummy Station 1",
            description="Dummy Station 1 located somewhere",
            longitude_latitude="SRID=4326;POINT (18.412905 18.005895)",
            contact="foufoutos@yahoo.gr",
            category=self.category
        )

    def test_data_unique_constraints(self):
        with self.assertRaises(IntegrityError):
            Station.objects.create(
                name="Dummy Station 1",
                description="Dummy Station 1 located somewhere",
                longitude_latitude="SRID=4326;POINT (18.412905 18.005895)",
                contact="foufoutos@yahoo.gr",
                category=self.category
            )

    def test_latlon_postgis_compliance_fail(self):
        with self.assertRaises(TypeError):
            Station.objects.create(
                name="Dummy Station 1",
                description="Dummy Station 1 located somewhere",
                longitude_latitude=(18.412905, 18.005895),
                contact="foufoutos@yahoo.gr",
                category=self.category
            )
