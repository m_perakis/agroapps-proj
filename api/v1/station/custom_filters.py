from django_filters import rest_framework as filters

from station.models import Station


class StationFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Station
        fields = ['category', ]


class CategoryFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Station
        fields = ['description', ]
