from django.test import TestCase

from station.models import Station
from category.models import Category


class StationAPITest(TestCase):

    def setUp(self):
        self.category = Category.objects.create(id=1, name="Dummy Category")
        self.station = Station.objects.create(
            name="Dummy Station 1",
            description="Dummy Station 1 located somewhere",
            longitude_latitude="SRID=4326;POINT (18.412905 18.005895)",
            contact="foufoutos@yahoo.gr",
            category=self.category
        )

    def test_get_stations_success(self):
        response = self.client.get(
            '/api/v1/station/',
        )
        self.assertEqual(response.status_code, 200)

    def test_get_stations_filtered(self):
        response = self.client.get(
            '/api/v1/station/?longitude_latitude=SRID=4326;POINT (18.412905 18.005895)&unit=km&radius=1&category=1',
        )
        self.assertEqual(response.status_code, 200)

    def test_get_stations_filtered_fail(self):
        response = self.client.get(
            '/api/v1/station/?longitude_latitude=SRID=4326;POINT (18.412905, 18.005895)&unit=km&radius=1&category=1',
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["longitude_latitude"], "Wrong format ex. SRID=4326;POINT (23.6865234375 38.29855909225434)")

    def test_create_station_success(self):
        response = self.client.post(
            '/api/v1/user/register/',
            data={
                "email": "test@yahoo.gr",
                "username": "tester",
                "password": "test123456!"
            }
        )
        self.assertEqual(response.status_code, 201)

        response = self.client.post(
            '/api/v1/user/token/',
            data={
                "username": "tester",
                "password": "test123456!"
            },
        )
        self.assertEqual(response.status_code, 200)

        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + response.json()["access"],
        }
        response = self.client.post(
            '/api/v1/station/',
            data={
                "name": "Wind Station Aliartos",
                "description": "Wind Station 2 located in Aliartos, Viotia",
                "longitude_latitude": "SRID=4326;POINT (23.6865234375 38.29855909225434)",
                "contact": "foufoutos@yahoo.gr",
                "category": 1
            },
            **auth_headers
        )
        self.assertEqual(response.status_code, 201)

    def test_create_station_unauthorized(self):
        response = self.client.post(
            '/api/v1/station/',
            data={
                "name": "Wind Station 2 Aliartos",
                "description": "Wind Station 2 located in Aliartos, Viotia",
                "longitude_latitude": "SRID=4326;POINT (18.412905 18.005895)",
                "contact": "foufoutos@yahoo.gr",
                "category": 2
            }
        )
        self.assertEqual(response.status_code, 401)
