from django.contrib.gis.geos import Point
from rest_framework import serializers

from AgroApps import settings
from station.models import Station
from category.models import Category


class StationModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Station
        fields = "__all__"


class CategoryModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = "__all__"


class StationLatLonParamsValidator(serializers.Serializer):

    UNITS_TO_KM = {
        "ft": 0.0003048,
        "km": 1.0,
        "m": 0.001,
        "mi": 1.60934,
        "yd": 0.0009144,
    }

    def to_internal_value(self, data):
        lanlot = data.get("longitude_latitude")
        radius = data.get("radius", settings.LATLON_RADIUS)
        distance_unit = data.get("unit", settings.LATLON_RADIUS_UNIT).lower()
        validated_data = {}

        if lanlot:
            try:
                lat_lon_ls = lanlot.split(";")
                srid = int(lat_lon_ls[0].split("=")[1])
                point = eval(lat_lon_ls[1].split(maxsplit=1)[1].replace(" ", ","))
                validated_data["longitude_latitude"] = Point(x=point[0], y=point[1], srid=srid)
            except Exception:
                raise serializers.ValidationError(
                    {
                        "longitude_latitude": "Wrong format ex. SRID=4326;POINT (23.6865234375 38.29855909225434)",
                    }
                )

            try:
                radius = float(radius)
            except Exception:
                raise serializers.ValidationError(
                    {
                        "radius": "Wrong format please provide an integer or a float",
                    }
                )

            if distance_unit in self.UNITS_TO_KM.keys():
                validated_data["wgs84_degrees"] = (self.UNITS_TO_KM[distance_unit]*radius/40000 * 360)
            else:
                raise serializers.ValidationError(
                    {
                        "unit": f"Wrong format please provide one of these {self.UNITS_TO_KM.keys()}",
                    }
                )

        return validated_data
