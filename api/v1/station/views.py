from django.db.models import QuerySet
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters import rest_framework as filters

from station.models import Station
from . import serializers
from . import custom_filters
from .serializers import StationLatLonParamsValidator

longitude_latitude = openapi.Parameter(
    'longitude_latitude', in_=openapi.IN_QUERY,
    type=openapi.TYPE_STRING
)
unit = openapi.Parameter(
    'unit', in_=openapi.IN_QUERY,
    type=openapi.TYPE_STRING
)
radius = openapi.Parameter(
    'radius', in_=openapi.IN_QUERY,
    type=openapi.TYPE_NUMBER
)


class StationAPI(ListCreateAPIView):
    serializer_class = serializers.StationModelSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = custom_filters.StationFilter
    permission_classes = [IsAuthenticatedOrReadOnly, ]

    def get_queryset(self):
        lat_lon_validator = StationLatLonParamsValidator(data=self.request.query_params)
        lat_lon_validator.is_valid(raise_exception=True)
        validated_lat_lon = lat_lon_validator.validated_data

        queryset = Station.objects.all()

        longitude_latitude = validated_lat_lon.get("longitude_latitude")
        wgs84_degrees = validated_lat_lon.get("wgs84_degrees")
        if all([longitude_latitude, wgs84_degrees]):
            queryset = queryset.filter(longitude_latitude__dwithin=(longitude_latitude, wgs84_degrees))

        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            queryset = queryset.all()
        return queryset

    @swagger_auto_schema(
        operation_description=(
                "longitude_latitude = ex. SRID=4326;POINT (18.412905 18.005895)\n"
                "unit = ex. km, mi, m etc."
        ),
        manual_parameters=[longitude_latitude, unit, radius],
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class CategoryAPI(ListCreateAPIView):
    serializer_class = serializers.CategoryModelSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = custom_filters.CategoryFilter
    permission_classes = [IsAuthenticatedOrReadOnly, ]
