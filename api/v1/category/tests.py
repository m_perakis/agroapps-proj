from django.test import TestCase

from category.models import Category


class CategoryAPITest(TestCase):

    def test_get_category_success(self):
        response = self.client.get(
            '/api/v1/category/',
        )
        self.assertEqual(response.status_code, 200)

    def test_get_category_filtered(self):
        response = self.client.get(
            '/api/v1/category/?name=Dummy Category',
        )
        self.assertEqual(response.status_code, 200)

    def test_create_category_success(self):
        response = self.client.post(
            '/api/v1/user/register/',
            data={
                "email": "test@yahoo.gr",
                "username": "tester",
                "password": "test123456!"
            }
        )
        self.assertEqual(response.status_code, 201)

        response = self.client.post(
            '/api/v1/user/token/',
            data={
                "username": "tester",
                "password": "test123456!"
            },
        )
        self.assertEqual(response.status_code, 200)

        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + response.json()["access"],
        }
        response = self.client.post(
            '/api/v1/category/',
            data={
                "name": "foofootos",
                "description": "blablabla"
            },
            **auth_headers
        )
        self.assertEqual(response.status_code, 201)

    def test_create_category_unauthorized(self):
        response = self.client.post(
            '/api/v1/category/',
            data={
                "name": "foofootos",
                "description": "blablabla"
            },
        )
        self.assertEqual(response.status_code, 401)
