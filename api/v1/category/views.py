from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters import rest_framework as filters

from category.models import Category
from . import serializers
from . import custom_filters


class CategoryAPI(ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = serializers.CategoryModelSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = custom_filters.CategoryFilter
    permission_classes = [IsAuthenticatedOrReadOnly, ]
