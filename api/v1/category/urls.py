from django.urls import path
from . import views


urlpatterns = [
    path('', views.CategoryAPI.as_view()),
]
