from django_filters import rest_framework as filters

from category.models import Category


class CategoryFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Category
        fields = ['description', ]
