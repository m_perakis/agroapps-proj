from django.urls import path, include


urlpatterns = [
    path('user/', include('api.v1.user.urls')),
    path('station/', include('api.v1.station.urls')),
    path('category/', include('api.v1.category.urls'))
]