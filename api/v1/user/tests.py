from django.test import TestCase


class RegisterViewTest(TestCase):

    def test_register_login_success(self):
        response = self.client.post(
            '/api/v1/user/register/',
            data={
                "email": "test@yahoo.gr",
                "username": "tester",
                "password": "test123456!"
            }
        )
        self.assertEqual(response.status_code, 201)

        response = self.client.post(
            '/api/v1/user/token/',
            data={
                "username": "tester",
                "password": "test123456!"
            },
        )
        self.assertEqual(response.status_code, 200)

    def test_register_fail(self):
        response = self.client.post(
            '/api/v1/user/register/',
            data={
                "email": "test@yahoo.gr",
                "password": "test123456!"
            }
        )
        self.assertEqual(response.status_code, 400)

    def test_login_unauthorized(self):
        response = self.client.post(
            '/api/v1/user/token/',
            data={
                "username": "test@yahoo.gr",
                "password": "test123456!"
            }
        )
        self.assertEqual(response.status_code, 401)
