# AgroApps-proj

Dockerized Django project consisted by several apps.

Essentially a basic API to serve geospatial data.

Main tools used:

Django (Python web framework)

PostGIS (Postgresql extension)

Basic prerequisites:

Install Docker platform

## Getting started

Run in a terminal the following commands:

sudo docker-compose up -d  --build

sudo docker-compose exec web python manage.py migrate

sudo docker-compose exec web python manage.py createsuperuser

Then Open the url http://localhost/admin/ and provide your credentials which you recently created.

Hit http://localhost/apidoc-swagger/ to check API's available endpoints and use.
